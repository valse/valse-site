{% extends 'views/_template.wala' %}

{% zone title %}
Valse
{% endzone %}

{% zone body %}
    <div class="presenter">
        <img src="valse.png" alt="Valses logo">
        <h1>Valse</h1>
        <h2>A MVC web framework for Vala</h2>
    </div>
    <div id="links" class="row">
        <a href="/doc/">Documentation</a>
        <a href="https://framagit.org/valse/valse">See the code</a>
        <a href="https://zestedesavoir.com/forums/sujet/5573/valse/">Topic on Zeste de Savoir</a>
    </div>
    <div class="row">
        <h2>What is Valse?</h2>
        <p>
            Valse is a free (as in freedom) MVC framework for the Vala programming langage, itself written in Vala.
            It let you create fast and modern web applications in a few time.
        </p>
        <p>
            Because it use the MVC architecture, your code will always stay clean and organized. It provides a powerfull
            templating engine, named Wala, a SQLite database management utility and all you need to create a dynamic website and
            keep your code concise and comprehensible.
        </p>
        <p>
            You can use it as a standalone application, or with a web server like Apache or Nginx.
        </p>
    </div>
    <div class="row">
        <h2>Features</h2>
        <div id="features">
            <div>
                <h3>Multiplatform</h3>
                <p>
                    You can use Valse both on Linux, Windows or Mac OS X. You can run your website almost everywhere,
                    from the little Rasberry Pi, to the server farm.
                </p>
            </div>
            <div>
                <h3>100% Vala</h3>
                <p>
                    Valse have been written in Vala, and have been designed to work with this language.
                    So you can create a website using a lot of great libraries and a high-level and high-performance language.
                </p>
            </div>

            <div>
                <h3>Fast</h3>
                <p>
                    Valse isn't based on an interpreted language, unlike Django, Express, Asp.NET, Laravel, Balaidor, Ruby
                    on Rails and many more. Your website will be the fastest!
                </p>
            </div>
            <div>
                <h3>Free software</h3>
                <p>
                    Valse is under the LGPLv3 license. It means that everybody can see and modify the code.
                    You can use it for free, without any authorization. You can also create your own version of Valse.
                </p>
            </div>
            <div>
                <h3>You have the power</h3>
                <p>
                    Almost evrything in Valse is customizable. You don't like the way static files are served: change it.
                    You need a Wala filter that doesn't exist yet: create it. You want to create your own forms: no problems,
                    Valse will not create them automatically from your models.
                </p>
            </div>
        </div>
    </div>
    <div class="row">
        <h2>Meet Wala</h2>
        <p>Wala is the default render engine of Valse. You can render dynamic pages elegantly.</p>
        <h3>Example</h3>
        <p>
            Put an action similar to this one in your controller.
        </p>
        <pre>
<code>public Response index () {
    Profile user = new Profile ();
    user.name = "John Doe";
    user.followers = 34;
    user.description = "I'm someone.";
    return page ("mypage.wala", model ("user", user), { "register" });
}</code>
        </pre>
        <p>
            Then, in the file <code>mypage.wala</code> put the following code.
        </p>
        <pre>
<code>&lt;h1&gt;&#123;&#123; user.name|upper &#125;&#125;&lt;/h1&gt;
&lt;p&gt;Description : &#123;&#123; user.description &#125;&#125;&lt;/p&gt;
&lt;p&gt;This user has &#123;&#123; user.followers &#125;&#125; followers!&lt;/p&gt;
&#123;% <span class="type">tag</span> <span class="str">'register'</span> %&#125;

&lt;p&gt;Create your own account and start sharing with &#123;&#123; user.name &#125;&#125;!&lt;/p&gt;

&#123;% <span class="type">endtag</span> %&#125;</code>
        </pre>
        <p>You'll get an HTML code similar to this one.</p>
        <pre>
<code>&lt;h1&gt;JOHN DOE&lt;/h1&gt;
&lt;p&gt;Description : I'm someone.&lt;/p&gt;
&lt;p&gt;This user has 34 followers!&lt;/p&gt;
&lt;p&gt;Create your own account and start sharing with John Doe!&lt;/p&gt;</code>
        </pre>
    </div>
    <div class="row center">
        <h2>Interessed?</h2>
        <a href="./doc/tutorial/getting-started/" class="button">Get started!</a>
    </div>
{% endzone %}
