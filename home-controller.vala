using Valse;
using Gee;

public class HomeControler : Controller {

    public HomeControler () {
        this.add_action ("index", this.index);
        this.add_action ("poulp", this.poulp);
    }

    public Response index () {
        return page ("views/home.wala", new HashMap<string, Object> (), { "home" });
    }

    public Response poulp () {
        return page ("views/poulp.wala", new HashMap<string, Object> (), { "home" });
    }
}
