public class TutorialModel : Object {

    public TutorialModel (string file, string title, string id) {
        try {
            string file_content = "Error while reading %s.".printf (file);
            GLib.FileUtils.get_contents (file, out file_content);
            this.content = file_content;
        } catch {
            print ("Error whle reading %s.\n", file);
        }
        this.title = title;
        this.id = id;
    }

    public string id {get; set;}

    public string title {get; set;}

    public string content {get; set;}

}
