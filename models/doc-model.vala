public class DocModel : Object {

    public DocModel (string title, string file, string base_url) {
        this.title = title;
        this.file = file;
        this.fileurl = base_url + file + ".vala";
        this.html = doc_html (download (this.fileurl));
    }

    public string title { get; set; }

    public string html { get; set; }

    public string file { get; set; }

    public string fileurl { get; set; }


    private string doc_html (string vala) {
        string res = "";
        string[] lines = vala.split("\n");

        int i = 0;
        foreach (var line in lines) {
            string desc = "<p>";
            if (line.contains("/**")) {
                // On entre dans un commentaire
                int j = i + 1;
                string nextLine = lines[j].replace("\t", "").strip ();
                string params = "";
                string returns = "";
                while (nextLine.index_of("*") == 0) {
                    // tant qu'on est dans le commentaire
                    if (nextLine == "*/") {
                        // On arrive a la fin du com
                        // on affiche tous les taglets collectés
                        bool show_taglets = params.length > 0 || returns.length > 0;

                        if (show_taglets) {
                            desc += "<div class=\"taglets\">";
                        }

                        if (params.length > 0) {
                            desc += "<h3>Parameters</h3>";
                            desc += params;
                            params = "";
                        }
                        if (returns.length > 0) {
                            desc += "<h3>Returns</h3>";
                            desc += returns;
                            returns = "";
                        }

                        if (show_taglets) {
                            desc += "</div>";
                        }
                        break;
                    }

                    string compute = nextLine.replace("*", "");

                    try {
                        // associe un namespace avec un package
                        // TODO : faire un recherche sur valadoc et prendre le 1er resultat
                        Gee.HashMap<string, string> packages = new Gee.HashMap<string, string> ();
                        packages ["GLib"] = "glib-2.0";
                        packages ["Soup"] = "libsoup-2.4";
                        packages ["Gee"] = "gee-0.8";
                        packages ["Sqlite"] = "sqlite3";
                        packages ["Cairo"] = "cairo";

                        Regex link_re = new Regex ("{ *@link (?P<ref>[[:graph:]]*) *}");
                        MatchInfo info;
                        bool go = link_re.match (compute, 0, out info);

                        while (go) {
                            string href = info.fetch_named ("ref");
                            string adress = "#";

                            if (Regex.match_simple ("^Valse", href)) {
                                // on renvoie vers notre propre doc
                                string[] link_split = href.split (".");
                                adress = link_split [1].down ();
                                if (link_split.length == 2) {
                                    adress += "#" + link_split [2];
                                }
                            } else {
                                // on renvoie sur valadoc
                                string package = packages [href.split (".") [0]];
                                adress = "http://valadoc.org/" + package + "/" + href + ".html";
                            }

                            string[] link_split = href.split (".");
                            string name = "";
                            for (int k = 0; k < link_split.length; k++) {
                                if (k > 0) {
                                    if (name.length > 0) {
                                        name += "." + link_split [k];
                                    } else {
                                        name += link_split [k];
                                    }
                                }
                            }

                            string link = "<a href=\"" + adress + "\">" + name + "</a>";
                            compute = link_re.replace (compute, compute.length, 0, link);

                            go = info.next ();
                        }
                    } catch {
                        res = "Error while retriving documentation.";
                    }


                    if (compute.length > 0) {
                        if (Regex.match_simple ("^ @", compute)) {
                            // Taglet
                            string tag_type = compute.split (" ") [1];
                            switch (tag_type) {
                                case "@param":
                                    string param_name = compute.split (" ") [2];
                                    params += "<strong>" + param_name + "</strong>" + "<p class=\"param\"> " + compute.replace (" @param " + param_name + " ", "") + "</p>";
                                    break;
                                case "@return":
                                    returns += compute.replace ("@return ", "");
                                    break;
                                default:
                                    desc += compute;
                                    break;
                            }

                            //desc += "<p ></em>";
                        } else {
                            desc += compute;
                        }

                    } else {
                        desc += "</p><p>";
                    }
                    j++;
                    nextLine = lines[j].replace("\t", "").strip ();
                }
                j++;
                nextLine = lines[j].replace("\t", "").strip ();
                string dec = nextLine;
                dec = dec.replace ("<", "&lt");
                dec = dec.replace (">", "&gt");
                if (nextLine.has_prefix ("public ")) {
                    res += "<pre class=\"def\"><code class=\"hljs vala\">%s</code></pre>".printf (dec);
                    res += desc;
                }
            }
            i++;
        }
        if (res == "") {
            res = "<p>It look likes nothing is documented for %s</p>".printf (this.title);
        }

        return res;
    }

    public string download (string url) {
        string res = "";
    	File file = File.new_for_uri (url);
    	try {
    		DataInputStream dis = new DataInputStream (file.read ());
    		string line;

    		while ((line = dis.read_line ()) != null) {
    			res += line + "\n";
    		}
    	} catch (Error e) {
    		print ("Error: %s\n", e.message);
    	}

    	return res;
    }

}
