using Valse;

public class DocHomeModel : Object {

    public Many<DocModel> reference { get; set; default = new Many<DocModel> (); }

    public Many<TutorialModel> tutorials { get; set; default = new Many<TutorialModel> (); }

}
