using Valse;

void main (string [] args) {
    Router r = new Router ();
    r.register ("home", new HomeControler ());
    r.register ("doc", new DocController());
    r.listen ();
}
