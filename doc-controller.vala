using Valse;
using Gee;

public class DocController : Controller {

    private string file_url = "https://framagit.org/valse/valse/raw/master/";

    private TutorialModel[] tuto_repo = new TutorialModel[] {};
    private DocModel[] doc_repo = new DocModel[] {};

    public DocController () {
        this.add_action ("index", this.index);
        this.add_complete_action ("reference", this.reference, { "title" });
        this.add_complete_action ("tutorial", this.tuto, { "id" });
        this.add_complete_action ("demo", this.demo, { "id" });

        this.tuto_repo += new TutorialModel ("views/tuto/getting-started.wala", "Getting started with Valse", "getting-started");
        this.tuto_repo += new TutorialModel ("views/tuto/wala.wala", "Mastering Wala", "mastering-wala");

        this.doc_repo += new DocModel ("Router", "valse/core/router", file_url);
        this.doc_repo += new DocModel ("Action", "valse/core/action", file_url);
        this.doc_repo += new DocModel ("RouterOptions", "valse/core/router-options", file_url);
        this.doc_repo += new DocModel ("Controller", "valse/core/controller", file_url);
        this.doc_repo += new DocModel ("DataBase", "valse/core/database", file_url);
        this.doc_repo += new DocModel ("Request", "valse/core/request", file_url);
        this.doc_repo += new DocModel ("Response", "valse/core/response", file_url);
        this.doc_repo += new DocModel ("Many", "valse/wala/many", file_url);
        this.doc_repo += new DocModel ("WalaFilter", "valse/wala/wala-filter", file_url);
    }

    /**
    * L'accueil de la doc, qui liste les tutos et les éléments de la référence
    */
    public Response index () {
        var tutos = new Many<TutorialModel>.wrap (this.tuto_repo);
        var docs = new Many<DocModel>.wrap (this.doc_repo);

        return page ("views/doc-home.wala", model ("tutos", tutos, "docs", docs));
    }

    /**
    * Les exemples des tutos
    */
    public Response demo (Request req, Response res) {
        return page ("views/demo/" + req.query_ids ["id"] + ".html");
    }

    public Response tuto (Request req, Response res) {
        if (req.query_ids ["id"] == null || req.query_ids ["id"].length == 0) {
            var doc_model = new DocHomeModel ();
            doc_model.tutorials = new Many<TutorialModel>.wrap (this.tuto_repo);
            doc_model.reference = new Many<DocModel>.wrap (this.doc_repo);
            return page ("views/tuto/index.wala", model ("mod", doc_model));
        }

        foreach (var tuto in this.tuto_repo) {
            if (tuto.id == req.query_ids ["id"]) {
                return template (tuto.content);
            }
        }
        return page ("views/error/404.wala");
    }

    public Response reference (Request req, Response res) {
        foreach (var doc in this.doc_repo) {
            if (doc.title.down () == req.query_ids ["title"].down ()) {
                res.body = page ("views/doc.wala", model ("mod", doc)).body;
                return res;
            }
        }
        res.body = page ("views/error/404.wala").body;
        return res;
    }
}
