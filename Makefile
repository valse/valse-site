# Add .exe if you are on Windows
OUT = site
LINUXLIB = --pkg valse --pkg libscgi-1.0 -X "-I." -X libscgi-1.0.so --pkg gee-0.8 --pkg libsoup-2.4 -X "-I." -X valse.o
WINDOWSLIB = --pkg libscgi-1.0 -X "-I." -X libscgi-1.0.dll --pkg libsoup-2.4 --pkg gee-1.0 --pkg sqlite3
FILES = *.vala models/*.vala

.ONESHELL:
linux:
	export LD_LIBRARY_PATH=.
	valac --vapidir=. $(LINUXLIB) $(FILES) -o $(OUT)
	./$(OUT)

windows:
	valac --vapidir=. $(WINDOWSLIB) $(FILES) $(LIBOUT) -o $(WINOUT) -X -w

clean:
	rm $(OUT)
